﻿using System.Collections.Generic;
using System.Linq;

namespace AdsDistributor
{
    public class GreedySolver : IAdsDistributorTaskSolver
    {
        public int[] Solve(double[,] distanceMatrix, int startCity)
        {
            Guard.ArgumentNotNull(distanceMatrix, nameof(distanceMatrix));

            var n = distanceMatrix.GetLength(0);
            var m = distanceMatrix.GetLength(1);

            Guard.ArgumentValid(n == m, "matrix is not square", nameof(distanceMatrix));

            Guard.ArgumentInRange(startCity >= 0 && startCity < n, 
                "startCity must be >= 0 and < n", nameof(startCity));

            var visited = new HashSet<int>();
            var profits = new double[n];

            var currentCity = startCity;
            visited.Add(currentCity);

            var currentDistance = 0.0;
            var path = new List<int>();

            for (var i = 1; i < n; i++)
            {
                var nearestCity = 0;
                while (nearestCity == currentCity || visited.Contains(nearestCity))
                {
                    nearestCity++;
                }

                for (var city = nearestCity; city < n; city++)
                {
                    if (city == currentCity || visited.Contains(city))
                    {
                        continue;
                    }

                    if (distanceMatrix[currentCity, nearestCity] > distanceMatrix[currentCity, city])
                    {
                        nearestCity = city;
                    }
                }

                path.Add(nearestCity);
                currentDistance += distanceMatrix[currentCity, nearestCity];
                profits[i] = 100.0 * i - currentDistance - distanceMatrix[nearestCity, startCity];

                currentCity = nearestCity;
                visited.Add(nearestCity);
            }
            
            var bestCitiesCount = 0;
            for (var i = 1; i < n; i++)
            {
                if (profits[i] > profits[bestCitiesCount])
                {
                    bestCitiesCount = i;
                }
            }

            return path
                .Take(bestCitiesCount)
                .ToArray();
        }

        public int[] Solve(double[,] distanceMatrix)
        {
            return Solve(distanceMatrix, 0);
        }
    }
}
