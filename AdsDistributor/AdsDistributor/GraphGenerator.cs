﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdsDistributor
{
    public class GraphGenerator
    {
        private IEnumerable<Tuple<int, int>> GetEdges(int n)
        {
            var indexes = Enumerable.Range(0, n).ToArray();
            return from i in indexes
                   from j in indexes
                   where i != j
                   select Tuple.Create(i, j);
        }

        public double[,] GenerateDistanceMatrix(int n, double pMin, double pMax)
        {
            Guard.ArgumentInRange(n >= 2, "n must be >= 2", nameof(n));
            Guard.ArgumentInRange(pMin > 0 && pMin < 100000, "pMin must be in (0; 10^5)", nameof(pMin));
            Guard.ArgumentInRange(pMax > 0 && pMax < 100000, "pMax must be in (0; 10^5)", nameof(pMax));
            Guard.ArgumentValid(pMin <= pMax, "pMin must be <= pMax", nameof(pMin));

            var m = n * (n - 1);
            var d = (pMax - pMin) / (m - 1);
            var edges = GetEdges(n).ToArray();

            var random = new Random();
            random.Shuffle(edges);

            var distanceMatrix = new double[n, n];
            for (var i = 0; i < m; i++)
            {
                var edge = edges[i];
                distanceMatrix[edge.Item1, edge.Item2] = pMin + i * d;
            }

            return distanceMatrix;
        }
    }
}
