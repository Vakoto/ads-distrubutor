﻿using NUnit.Framework;

namespace AdsDistributor
{
    [TestFixture]
    internal class GreedySolverTests
    {
        [Test]
        public void SolveTest_6x6()
        {
            var distanceMatrix = new double[,]
            {
                { 0,   80,   5,    7,   66,  90  },
                { 95,  0,    100,  96,  82,  12  },
                { 6,   88,   0,    10,  74,  79  },
                { 8,   110,  2,    0,   69,  120 },
                { 66,  82,   84,   69,  0,   73  },
                { 77,  18,   99,   70,  73,  0   }
            };

            var expectedPath = new[] { 2, 3, 4, 5, 1 };

            Test(distanceMatrix, expectedPath);
        }

        [Test]
        public void SolveTest_2x2()
        {
            var distanceMatrix = new double[,]
            {
                { 0, 1 },
                { 1, 0 }
            };

            var expectedPath = new[] { 1 };

            Test(distanceMatrix, expectedPath);
        }

        private void Test(double[,] distanceMatrix, int[] expectedPath)
        {
            var solver = new GreedySolver();
            var actualPath = solver.Solve(distanceMatrix);
            CollectionAssert.AreEqual(expectedPath, actualPath);
        }
    }
}
