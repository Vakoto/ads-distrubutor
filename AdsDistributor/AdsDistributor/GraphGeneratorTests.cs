﻿using System;
using FluentAssertions;
using NUnit.Framework;

namespace AdsDistributor
{
    [TestFixture]
    internal class GraphGeneratorTests
    {
        private GraphGenerator graphGenerator;

        [SetUp]
        public void SetUp()
        {
            graphGenerator = new GraphGenerator();
        }

        [Test]
        public void ShouldThrowsExceptionsOnInvalidArguments()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => graphGenerator.GenerateDistanceMatrix(1, 5, 10));
            Assert.Throws<ArgumentOutOfRangeException>(() => graphGenerator.GenerateDistanceMatrix(5, 0, 10));
            Assert.Throws<ArgumentOutOfRangeException>(() => graphGenerator.GenerateDistanceMatrix(5, 5, 0));
            Assert.Throws<ArgumentException>(() => graphGenerator.GenerateDistanceMatrix(5, 10, 5));
        }
        
        [Test]
        public void ShouldGenerateValidDistanceMatrix()
        {
            const int pMin = 5;
            const int pMax = 10;
            for (var n = 2; n < 10; n++)
            {
                var matrix = graphGenerator.GenerateDistanceMatrix(n, pMin, pMax);
                matrix.GetLength(0).Should().Be(n);
                matrix.GetLength(1).Should().Be(n);
                for (var i = 0; i < n; i++)
                {
                    for (var j = 0; j < n; j++)
                    {
                        if (i == j)
                        {
                            matrix[i, j].Should().Be(0.0);
                        }
                        else
                        {
                            matrix[i, j].Should().BeInRange(pMin, pMax);
                        }
                    }
                }
            }
        }
    }
}
