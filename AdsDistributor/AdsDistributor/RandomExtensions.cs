﻿using System;

namespace AdsDistributor
{
    internal static class RandomExtensions
    {
        public static void Shuffle<T>(this Random random, T[] array)
        {
            var n = array.Length;
            while (n > 1)
            {
                var k = random.Next(n--);
                var temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }
    }
}
