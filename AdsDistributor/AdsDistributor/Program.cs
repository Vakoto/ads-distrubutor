﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace AdsDistributor
{
    class Program
    {
        private static double CalculateProfit(double[,] distanceMatrix, int[] path, int startCity = 0)
        {
            var totalDistance = 0.0;
            var currentCity = startCity;
            foreach (var city in path)
            {
                totalDistance += distanceMatrix[currentCity, city];
                currentCity = city;
            }
            return 100.0 * path.Length - totalDistance - distanceMatrix[currentCity, startCity];
        }

        private static void GenerateDistanceMatrix(int n, double pMin, double pMax, string fileName)
        {
            var distanceMatrix = new GraphGenerator().GenerateDistanceMatrix(n, pMin, pMax);
            using (var sw = new StreamWriter(fileName))
            {
                sw.WriteLine(n);
                for (var i = 0; i < n; i++)
                {
                    for (var j = 0; j < n; j++)
                    {
                        sw.Write(distanceMatrix[i, j].ToString("0.000", CultureInfo.InvariantCulture));
                        if (j < n - 1)
                        {
                            sw.Write(' ');
                        }
                    }
                    sw.WriteLine();
                }
            }
        }

        private static double[,] ReadMatrix(string fileName)
        {
            using (var sw = new StreamReader(fileName))
            {
                var n = int.Parse(sw.ReadLine());

                var matrix = new double[n, n];
                for (var i = 0; i < n; i++)
                {
                    var line = sw.ReadLine();

                    var numbers = line
                        .Trim()
                        .Replace(',', '.')
                        .Split(' ', '\t')
                        .Select(l => double.Parse(l, CultureInfo.InvariantCulture))
                        .ToArray();

                    for (var j = 0; j < n; j++)
                    {
                        matrix[i, j] = numbers[j];
                    }
                }

                return matrix;
            }
        }

        private static int Menu(string[] menuOptions)
        {
            while (true)
            {
                var i = 1;
                foreach (var menuOption in menuOptions)
                {
                    Console.WriteLine($"{i}:\t{menuOption}");
                    i++;
                }
                Console.Write("Ввод: ");
                var line = Console.ReadLine();
                if (int.TryParse(line, out var result))
                {
                    return result;
                }
                Console.WriteLine("Ошибка ввода.");
                Console.WriteLine();
            }
        }

        private static void GenerateDistanceMatrix()
        {
            Console.Write("Имя файла: ");
            var fileName = Console.ReadLine();

            Console.Write("N (2 <= N <= 10000): ");
            var n = int.Parse(Console.ReadLine());


            Console.Write("pMin (0 < pMin < 10^5): ");
            var pMin = double.Parse(Console.ReadLine());

            Console.Write("pMax (0 < pMin <= pMax < 10^5): ");
            var pMax = double.Parse(Console.ReadLine());

            GenerateDistanceMatrix(n, pMin, pMax, fileName);
        }

        private static void Main(string[] args)
        {
            var menuOptions = new[]
            {
                "Сгенерировать cлучайный полный ориентированный граф с линейно-равномерным распределением длин ребер.",
                "Помочь распространителю рекламы (жадный алгоритм).",
                "95 (файлы вида input{i}.txt)",
                "Выход."
            };
            var needRepeat = true;
            while (needRepeat)
            {
                switch (Menu(menuOptions))
                {
                    case 1:
                        try
                        {
                            GenerateDistanceMatrix();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Ошибка: {ex.Message}");
                        }
                        break;
                    case 2:
                        try
                        {
                            HelpDistributor("input.txt", "output.txt");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Ошибка: {ex.Message}");
                        }
                        break;
                    case 3:
                        Console.Write("Количество файлов: ");
                        var n = int.Parse(Console.ReadLine());
                        for (var i = 1; i <= n; i++)
                        {
                            var inputFileName = $"input{i}.txt";
                            var outputFileName = $"output{i}.txt";
                            try
                            {

                                HelpDistributor(inputFileName, outputFileName);
                            }
                            catch (Exception e)
                            {   
                                Console.WriteLine($"Ошибка в файле {inputFileName}");
                                Console.WriteLine(e.Message);
                            }
                        }

                        break;
                    case 4:
                        needRepeat = false;
                        break;
                    default:
                        Console.WriteLine("Неверная опция.");
                        break;
                }

                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey();
                Console.Clear();
            }
        }

        private static void HelpDistributor(string inputFileName, string outputFileName)
        {
            Guard.ArgumentValid(File.Exists(inputFileName), $"Входной файл {inputFileName} не существует", nameof(inputFileName));

            var solver = new GreedySolver();
            var matrix = ReadMatrix(inputFileName);
            var resultPath = solver.Solve(matrix);
            var profit = CalculateProfit(matrix, resultPath);

            var startCity = new[] { 0 };
            var path = startCity
                .Concat(resultPath)
                .Concat(startCity)
                .ToArray();

            using (var sw = new StreamWriter(outputFileName))
            {
                sw.WriteLine(profit.ToString(CultureInfo.InvariantCulture));
                sw.WriteLine(string.Join(" ", path));
            }
        }
    }
}
