﻿namespace AdsDistributor
{
    public interface IAdsDistributorTaskSolver
    {
        int[] Solve(double[,] distanceMatrix);
    }
}